FROM debian:buster
RUN apt update && apt install -y \
    wget openjdk-11-jre-headless \
&& rm -rf /var/lib/apt/lists/*
RUN wget -q https://artifacts.elastic.co/downloads/logstash/logstash-7.8.0.deb && dpkg -i logstash-7.8.0.deb

WORKDIR /usr/share/logstash

ENV ELASTIC_CONTAINER true
ENV PATH=/usr/share/logstash/bin:$PATH

COPY config/pipelines.yml config/pipelines.yml
COPY config/logstash.yml config/logstash.yml
COPY config/logstash.yml /etc/logstash/logstash.yml
COPY config/log4j2.properties /etc/logstash/log4j2.properties
COPY config/log4j2.properties config/log4j2.properties

COPY pipeline/default.conf pipeline/logstash.conf


# Ensure Logstash gets a UTF-8 locale by default.
ENV LANG='en_US.UTF-8' LC_ALL='en_US.UTF-8'
COPY fix.sh .
RUN ./fix.sh

USER logstash

COPY docker-entrypoint /usr/local/bin/docker-entrypoint
# RUN chmod 0755 /usr/local/bin/docker-entrypoint

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
